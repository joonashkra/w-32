import React from "react";
import "./Tori.css";

const Tori = () => {
  return (
    <div className="tori">
      <div className="InputField">
        <input type="text" value="Hakusana ja/tai postinumero" />
        <select name="what">
          <option id="1">Kaikki osastot</option>
        </select>
        <select name="where">
          <option id="1">Koko Suomi</option>
        </select>
      </div>
      <div className="SearchField">
        <input type="checkbox" name="how" />
          <label>Myydään</label>
        <input type="checkbox" name="how" />
          <label>Ostetaan</label>
        <input type="checkbox" name="how" />
          <label>Vuokrataan</label>
        <input type="checkbox" name="how" />
          <label>Halutaan vuokrata</label>
        <input type="checkbox" name="how" />
          <label>Annetaan</label> 
        <label id="buttonLabel">Tallenna haku</label>
        <button>Hae</button>
      </div>
    </div>
  );
};

export default Tori;
